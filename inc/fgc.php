<?php



function fgc( $url, $timeout=null, $user_agent=null ){

	if(! $timeout )
		$timeout = 4;

	if(! $user_agent )
		$user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36';

	$fgct_header = [
		'http' => [
			'timeout' => intval($timeout),
			'header' => "User-Agent: ".$user_agent."\r\n",
		],
	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],
    ];
	$contx = stream_context_create($fgct_header);
	
	
	$c = @file_get_contents($url, false, $contx);
	
	if( substr($c, 0, 22) == "<br />\n<b>Warning</b>:" ){
		echo $c;
		return false;

	} else {
		return $c;
	}

}


